#!/bin/bash
# start stop script to run or kill all external nao controller

NAO_PID_FILE=/$HOME/run/nao_control.pid

function start {

  # check if nao software is already running
  if [ -f $NAO_PID_FILE ]
  then
    nao_control_PID=`cat $NAO_PID_FILE`
    isrunning=`ps x | grep $nao_control_PID | grep -v grep`
    if [ -n "$isrunning" ]
    then
      echo "nao controller is already running. Do not start it again!"
      return 1
    else
      # remove the pid file, if no process is running
      rm $NAO_PID_FILE
    fi
  fi


  # activate virtual ros env
  # On the EPFL server we need the ROS environment to make sure all ROS commands are available.
  source /opt/bbp/ros/ros_venv/bin/activate

  # source the ros hbp package
  source $ROS_HBP_PACKAGES_SETUP_FILE

  # source nao (only needed on dev server)
  #source ~/stauss/catkin_ws/devel/setup.bash
  # start the nao ROS controllers and save PID of the roslaunch process in a file
  roslaunch --pid=$NAO_PID_FILE nao_control nao_control_position.launch 2> /dev/null &

  # deactivate ros virtual env
  deactivate

  return 0
}

function stop {

  # activate virtual ros env
  # On the EPFL server we need the ROS environment to make sure all ROS commands are available.
  source /opt/bbp/ros/ros_venv/bin/activate

  # get nao control (roslaunch) PID from file and send kill command
  ROS_LAUNCH_PID=`cat $NAO_PID_FILE`
  if [ $ROS_LAUNCH_PID ]; then
    kill -INT $ROS_LAUNCH_PID
  fi

  # delete the pid storage file
  rm $NAO_PID_FILE


  # deactivate ros virtual env
  deactivate

  echo "Done stopping all nao control processes."

  return 0
}

mode=$1

case $mode in
'start')
    start
    exit 0
    ;;
'stop')
    stop
    exit 0
    ;;
*)
    echo "ERROR: nao_ext_controller - Unkown input mode $mode!"
    exit 1
    ;;
esac

