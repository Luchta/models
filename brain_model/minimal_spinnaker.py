# -*- coding: utf-8 -*-
"""
This file contains a minimal network
"""
# pragma: no cover

__author__ = 'Georg Hinkel'

from hbp_nrp_cle.brainsim import simulator as sim

left = sim.Population(5, sim.IF_curr_exp(), label="left")
right = sim.Population(5, sim.IF_curr_exp(i_offset=3.0), label="right")